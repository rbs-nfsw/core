package db

import "gitlab.com/rbs-nfsw/core/models/xml"

type Persona struct {
	ID       int
	UserID   int
	Name     string
	Cash     float32
	Boost    float32
	Presence int
}

func (p Persona) ProfileData() xml.ProfileData {
	profile := xml.ProfileData{}
	profile.PersonaID = p.ID
	profile.UserID = p.UserID
	profile.Name = p.Name
	profile.Cash = p.Cash
	profile.Boost = p.Boost
	return profile
}

func (p Persona) PersonaBase() xml.PersonaBase {
	out := xml.PersonaBase{}
	out.PersonaID = p.ID
	out.UserID = p.UserID
	out.Name = p.Name
	out.Presence = p.Presence
	out.Level = 2
	return out
}

func NewOwnedCar(c xml.OwnedCarTrans) OwnedCar {
	out := OwnedCar{}
	out.ID = c.ID
	out.Durability = c.Durability
	out.Heat = c.Heat
	out.BaseCar = c.CustomCar.BaseCar
	out.CarClassHash = c.CustomCar.CarClassHash
	out.Level = c.CustomCar.Level
	out.PhysicsProfileHash = c.CustomCar.PhysicsProfileHash
	out.Rating = c.CustomCar.Rating
	out.ResalePrice = c.CustomCar.ResalePrice
	out.RideHeightDrop = c.CustomCar.RideHeightDrop
	out.SkillModSlotCount = c.CustomCar.SkillModSlotCount
	out.Version = c.CustomCar.Version
	out.Paints = c.CustomCar.Paints
	out.Vinyls = c.CustomCar.Vinyls
	out.Name = c.CustomCar.Name
	out.PerformanceParts = c.CustomCar.PerformanceParts
	out.VisualParts = c.CustomCar.VisualParts
	return out
}

type OwnedCar struct {
	ID                 int
	PersonaID          int
	Durability         int
	Heat               float32
	BaseCar            int
	CarClassHash       int
	Level              int
	PhysicsProfileHash int
	Rating             int
	ResalePrice        float32
	RideHeightDrop     float32
	SkillModSlotCount  int
	Version            int
	Paints             string
	Vinyls             string
	Name               string
	PerformanceParts   string
	VisualParts        string
}

func (c OwnedCar) OwnedCarTrans() xml.OwnedCarTrans {
	out := xml.OwnedCarTrans{}
	out.ID = c.ID
	out.Durability = c.Durability
	out.Heat = c.Heat
	out.CustomCar.BaseCar = c.BaseCar
	out.CustomCar.CarClassHash = c.CarClassHash
	out.CustomCar.Level = c.Level
	out.CustomCar.PhysicsProfileHash = c.PhysicsProfileHash
	out.CustomCar.Rating = c.Rating
	out.CustomCar.ResalePrice = c.ResalePrice
	out.CustomCar.RideHeightDrop = c.RideHeightDrop
	out.CustomCar.SkillModSlotCount = c.SkillModSlotCount
	out.CustomCar.Version = c.Version
	out.CustomCar.Paints = c.Paints
	out.CustomCar.Vinyls = c.Vinyls
	out.CustomCar.Name = c.Name
	out.CustomCar.PerformanceParts = c.PerformanceParts
	out.CustomCar.VisualParts = c.VisualParts

	out.OwnershipType = "CustomizedCar"
	out.CustomCar.IsPreset = true
	return out
}
