package models

import "encoding/xml"

type ChatInfo struct {
	XMLName xml.Name   `xml:"chatServer"`
	Rooms   []ChatRoom `xml:"Rooms>chatRoom"`
	IP      string     `xml:"ip"`
	Port    int        `xml:"port"`
	Prefix  string     `xml:"prefix"`
}

type ChatRoom struct {
	Count     int
	ShortName string
	LongName  string
}
