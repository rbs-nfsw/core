package models

import (
	"encoding/xml"
	"io/ioutil"
)

func GetCarBasket(id string) OwnedCar {
	file, _ := ioutil.ReadFile("Baskets/" + id + ".xml")
	carTrans := OwnedCar{}
	xml.Unmarshal(file, &carTrans)
	return carTrans
}
