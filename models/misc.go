package models

type ServerInformation struct {
	Message                   string   `json:"messageSrv"`
	HomepageURL               string   `json:"homePageUrl"`
	FacebookURL               string   `json:"facebookUrl"`
	DiscordURL                string   `json:"discordUrl"`
	ServerName                string   `json:"serverName"`
	Country                   string   `json:"country"`
	Timezone                  int      `json:"timezone"`
	BannerURL                 string   `json:"bannerUrl"`
	AdminList                 string   `json:"adminList"`
	OwnerList                 string   `json:"ownerList"`
	NumberOfRegistered        int      `json:"numberOfRegistered"`
	AllowedCountries          []string `json:"allowedCountries"`
	ActivatedSceneryGroups    []string `json:"activatedHolidaySceneryGroups"`
	DisactivatedSceneryGroups []string `json:"disactivatedHolidaySceneryGroups"`
	OnlineNumber              int      `json:"onlineNumber"`
	RequireTicket             bool     `json:"requireTicket"`
	Version                   string   `json:"serverVersion"`
}

type HardwareInfo struct {
	AvailableMem        int    `xml:"availableMem"`
	CPUBrand            string `xml:"cpuBrand"`
	CPUID0              string `xml:"cpuid0"`
	CPUID10             int    `xml:"cpuid1_0"`
	CPUID11             int    `xml:"cpuid1_1"`
	CPUID12             int    `xml:"cpuid1_2"`
	CPUID13             int    `xml:"cpuid1_3"`
	DeviceID            int    `xml:"deviceID"`
	ExCPUID10           int    `xml:"excpuid1_0"`
	ExCPUID11           int    `xml:"excpuid1_1"`
	ExCPUID12           int    `xml:"excpuid1_2"`
	ExCPUID13           int    `xml:"excpuid1_3"`
	GpuDescription      int    `xml:"gpuDescription"`
	GpuDriverBuild      int    `xml:"gpuDriverBuild"`
	GpuDriverSubversion int    `xml:"gpuDriverSubversion"`
	GpuDriverVersion    int    `xml:"gpuDriverVersion"`
	GpuMemory           int    `xml:"gpuMemory"`
	GpuProduct          int    `xml:"gpuProduct"`
	OsBuildNumber       int    `xml:"osBuildNumber"`
	OsMajorVersion      int    `xml:"osMajorVersion"`
	OsMinorVersion      int    `xml:"osMinorVersion"`
	PhysicalCores       int    `xml:"physicalCores"`
	PlatformID          int    `xml:"platformID"`
	ProcessAffinityMask int    `xml:"processAffinityMask"`
	ServicePack         int    `xml:"servicePack"`
	SystemAffinityMask  int    `xml:"systemAffinityMask"`
	TotalMemory         int    `xml:"totalMemory"`
	UserID              int    `xml:"userID"`
	VendorID            int    `xml:"vendorID"`
}

type ExpLevelsMap []ExpLevelsMapEntry

func (m ExpLevelsMap) AsArray() []int {
	var maxLevel int
	mp := make(map[int]int)
	for _, e := range m {
		mp[e.Level] = e.Points
		if e.Level > maxLevel {
			maxLevel = e.Level
		}
	}
	out := make([]int, maxLevel)
	for i := range out {
		out[i] = mp[i]
	}
	return out
}

type ExpLevelsMapEntry struct {
	Level  int
	Points int
}
