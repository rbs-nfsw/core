package xml

import (
	x "encoding/xml"
	"io/ioutil"
)

type CustomCar struct {
	BaseCar            int
	CarClassHash       int
	Level              int
	PhysicsProfileHash int
	Rating             int
	ResalePrice        float32
	RideHeightDrop     float32
	SkillModSlotCount  int
	Version            int
	IsPreset           bool
	Name               string
	Paints             string `xml:",innerxml"`
	PerformanceParts   string `xml:",innerxml"`
	Vinyls             string `xml:",innerxml"`
	VisualParts        string `xml:",innerxml"`
}

type OwnedCarTrans struct {
	XMLName       x.Name `xml:"OwnedCarTrans"`
	CustomCar     CustomCar
	Durability    int
	Heat          float32
	OwnershipType string
	ID            int `xml:"Id"`
	PersonaID     int `xml:"-"`
}

func GetCarBasket(id string) OwnedCarTrans {
	file, _ := ioutil.ReadFile("Baskets/" + id + ".xml")
	carTrans := OwnedCarTrans{}
	x.Unmarshal(file, &carTrans)
	return carTrans
}

type ArrayOfOwnedCarTrans struct {
	V []OwnedCarTrans
}

type CarSlotInfoTrans struct {
	CarsOwnedByPersona   ArrayOfOwnedCarTrans
	DefaultOwnedCarIndex int
	OwnedCarSlotsCount   int
}

type InventoryTrans struct {
	PerformancePartsCapacity      int
	PerformancePartsUsedSlotCount int
	SkillModPartsCapacity         int
	SkillModPartsUsedSlotCount    int
	VisualPartsCapacity           int
	VisualPartsUsedSlotCount      int
}

type AchievementsPacket struct {
	PersonaID int `xml:"PersonaId"`
}

type ArrayOfLevelGiftDefinition struct {
}

type ProfileData struct {
	UserID            int     `xml:"-"`
	Boost             float32 `xml:"Boost"`
	Cash              float32 `xml:"Cash" bson:"cash"`
	IconIndex         int     `xml:"IconIndex" bson:"icon"`
	Level             int     `xml:"Level" bson:"-"`
	Name              string  `xml:"Name" bson:"name"`
	PercentToLevel    float32 `xml:"PercentToLevel" bson:"-"`
	PersonaID         int     `xml:"PersonaId" bson:"_id"`
	Rating            float32 `xml:"Rating" bson:"-"`
	Rep               float32 `xml:"Rep" bson:"-"`
	RepAtCurrentLevel int     `xml:"RepAtCurrentLevel" bson:"-"`
	Score             int     `xml:"Score" bson:"-"`
}

type ArrayOfBadges struct {
	V []string
}

type PersonaBase struct {
	Badges    ArrayOfBadges
	IconIndex int
	Level     int
	Name      string
	PersonaID int `xml:"PersonaId"`
	Score     int
	UserID    int `xml:"UserId"`
	Presence  int
}

type ArrayOfPersonaBase struct {
	V []PersonaBase `xml:"PersonaBase"`
}

type ProductTrans struct {
	XMLName        x.Name `xml:"ProductTrans"`
	Currency       string
	DurationMinute int
	Hash           int
	Icon           string
	Level          int
	Price          float32
	Priority       int
	ProductID      string `xml:"ProductId"`
	ProductTitle   string
	ProductType    string
	UseCount       int
}

type ArrayOfProductTrans struct {
	ProductTrans []ProductTrans
}

func LoadProductCategory(cat string) ArrayOfProductTrans {
	var out ArrayOfProductTrans
	b, err := ioutil.ReadFile("Products/" + cat + ".xml")
	check(err)
	check(x.Unmarshal(b, &out))
	//i, _ := x.Marshal(ArrayOfProductTrans{V: []ProductTrans{ProductTrans{}}})
	//fmt.Println(string(i))
	return out
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
