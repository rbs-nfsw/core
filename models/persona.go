package models

import (
	"github.com/shopspring/decimal"

	xmlm "gitlab.com/rbs-nfsw/core/models/xml"
)

type ProfileData struct {
	Boost             string
	Cash              string
	IconIndex         int
	Level             int
	Name              string
	PercentToLevel    float32
	PersonaID         int `xml:"PersonaId"`
	Rating            float32
	Rep               float32
	RepAtCurrentLevel int
	Score             int
}

type PersonaBase struct {
	Badges    xmlm.ArrayOfBadges
	IconIndex int
	Level     int
	Name      string
	PersonaID int `xml:"PersonaId"`
	Score     int
	UserID    int `xml:"UserId"`
	Presence  int
}

type Persona struct {
	ID     int
	UserID int
	Name   string
	Icon   int
	Cash   decimal.Decimal
	Boost  decimal.Decimal
}

func (p Persona) AsProfileData() ProfileData {
	return ProfileData{
		Boost:     p.Boost.String(),
		Cash:      p.Cash.String(),
		IconIndex: p.Icon,
		Level:     2,
		Name:      p.Name,
		PersonaID: p.ID,
	}
}

func (p Persona) AsPersonaBase() PersonaBase {
	return PersonaBase{
		IconIndex: p.Icon,
		Level:     2,
		Name:      p.Name,
		PersonaID: p.ID,
		UserID:    p.UserID,
		Presence:  1,
	}
}

type CarSlotInfoTrans struct {
	CarsOwnedByPersona   []OwnedCar `xml:"CarsOwnedByPersona>OwnedCarTrans"`
	DefaultOwnedCarIndex int
	OwnedCarSlotsCount   int
}
