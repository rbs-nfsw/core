package models

import (
	"encoding/xml"
	"time"

	sxt "github.com/jmoiron/sqlx/types"
)

type OwnedCar struct {
	XMLName       xml.Name  `xml:"OwnedCarTrans"`
	ID            int       `xml:"Id"`
	CustomCar     CustomCar `db:"-"`
	CustomCarID   int       `xml:"-" db:"custom_car"`
	Durability    int
	Heat          float32
	ExpDate       *time.Time
	OwnershipType string `db:"-"`
}

type CustomCar struct {
	ID               int `xml:"Id"`
	BaseCar          int
	CarClass         int `xml:"CarClassHash"`
	Level            int
	PhysicsProfile   int `xml:"PhysicsProfileHash"`
	Rating           int
	ResalePrice      float32
	HeightDrop       float32 `xml:"RideHeightDrop"`
	SkillSlotCount   int     `xml:"SkillModSlotCount"`
	Version          int
	IsPreset         sxt.BitBool
	Name             string
	Paints           []Paint           `xml:"Paints>CustomPaintTrans"`
	PerformanceParts []PerformancePart `xml:"PerformanceParts>PerformancePartTrans"`
	Vinyls           []Vinyl           `xml:"Vinyls>CustomVinylTrans"`
	VisualParts      []VisualPart      `xml:"VisualParts>VisualPartTrans"`
}

type Vinyl struct {
	XMLName   xml.Name `xml:"CustomVinylTrans"`
	ID        int      `xml:"-"`
	CustomCar int      `xml:"-"`
	Hash      int
	Hue1      int
	Hue2      int
	Hue3      int
	Hue4      int
	Layer     int
	Mir       sxt.BitBool
	Rot       int
	Sat1      int
	Sat2      int
	Sat3      int
	Sat4      int
	ScaleX    int `db:"scaleX"`
	ScaleY    int `db:"scaleY"`
	Shear     int
	TranX     int `db:"tranX"`
	TranY     int `db:"tranY"`
	Var1      int
	Var2      int
	Var3      int
	Var4      int
}

type Paint struct {
	XMLName   xml.Name `xml:"CustomPaintTrans"`
	ID        int      `xml:"-"`
	CustomCar int      `xml:"-"`
	Group     int
	Hue       int
	Sat       int
	Slot      int
	Var       int
}

type VisualPart struct {
	XMLName   xml.Name `xml:"VisualPart"`
	ID        int      `xml:"-"`
	CustomCar int      `xml:"-"`
	PartHash  int
	SlotHash  int
}

type PerformancePart struct {
	XMLName   xml.Name `xml:"PerformancePartTrans"`
	ID        int      `xml:"-"`
	CustomCar int      `xml:"-"`
	Hash      int      `xml:"PerformancePartAttribHash"`
}
