package models

type ArrayOfint struct {
	V []int `xml:"int"`
}

type ArrayOfLong struct {
	V []int `xml:"long"`
}

type ArrayOfPersonaBase struct {
	V []PersonaBase `xml:"PersonaBase"`
}
