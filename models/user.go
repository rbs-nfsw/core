package models

import (
	"time"

	sxt "github.com/jmoiron/sqlx/types"
)

type Token struct {
	Token  string
	UserID int
}

type User struct {
	ID             int
	Email          string
	LegacyPassword *string
	Password       *string
	IP             string
	CreatedAt      time.Time
	LastLoggedIn   time.Time
	Admin          sxt.BitBool
}

type LoginStatusVO struct {
	UserID      int `xml:"UserId"`
	LoginToken  string
	Description string
}
