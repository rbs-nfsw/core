package persona

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/rbs-nfsw/core/models"
)

type Repository interface {
	InsertPersona(persona models.Persona) (int, error)
	SelectUserPersonas(user int) ([]models.Persona, error)
	GetPersonaByID(id int) (models.Persona, error)
	GetPersonaOwnedCars(id int) ([]models.OwnedCar, error)
	InsertPersonaOwnedCar(id int, car models.OwnedCar) error
}

type sqlRepository struct {
	db *sqlx.DB
}

func NewSQLRepository(db *sqlx.DB) Repository {
	return sqlRepository{
		db: db,
	}
}

func (r sqlRepository) InsertPersona(persona models.Persona) (int, error) {
	res, err := r.db.NamedExec("insert into personas (user_id, name, icon, cash, boost) values (:user_id, :name, :icon, :cash, :boost)", persona)
	if err != nil {
		return 0, err
	}
	id, err := res.LastInsertId()
	return int(id), err
}

func (r sqlRepository) SelectUserPersonas(user int) ([]models.Persona, error) {
	var out []models.Persona
	err := r.db.Select(&out, "select * from personas where user_id = ?", user)
	return out, err
}

func (r sqlRepository) GetPersonaByID(id int) (models.Persona, error) {
	var out models.Persona
	err := r.db.Get(&out, "select * from personas where id = ?", id)
	return out, err
}

func (r sqlRepository) GetPersonaOwnedCars(id int) ([]models.OwnedCar, error) {
	out := make([]models.OwnedCar, 0)
	rows, err := r.db.Queryx("select owned_cars.* from (select * from car_slots where persona = ?) as cs inner join owned_cars on cs.owned_car = owned_cars.id", id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var ownedCar models.OwnedCar
		err := rows.StructScan(&ownedCar)
		if err != nil {
			return nil, err
		}
		err = r.db.Get(&ownedCar.CustomCar, "select * from custom_cars where id = ?", ownedCar.CustomCarID)
		if err != nil {
			return nil, err
		}
		tx, err := r.db.Beginx()
		if err != nil {
			return nil, err
		}
		err = tx.Select(&ownedCar.CustomCar.Vinyls, "select * from car_vinyls where custom_car = ?", ownedCar.CustomCarID)
		if err != nil {
			return nil, err
		}
		err = tx.Select(&ownedCar.CustomCar.Paints, "select * from car_paints where custom_car = ?", ownedCar.CustomCarID)
		if err != nil {
			return nil, err
		}
		err = tx.Select(&ownedCar.CustomCar.PerformanceParts, "select * from car_perf_parts where custom_car = ?", ownedCar.CustomCarID)
		if err != nil {
			return nil, err
		}
		err = tx.Select(&ownedCar.CustomCar.VisualParts, "select * from car_vis_parts where custom_car = ?", ownedCar.CustomCarID)
		if err != nil {
			return nil, err
		}
		err = tx.Commit()
		if err != nil {
			return nil, err
		}
		if ownedCar.CustomCar.IsPreset {
			ownedCar.OwnershipType = "PresetCar"
		} else {
			ownedCar.OwnershipType = "CustomizedCar"
		}
		out = append(out, ownedCar)
	}
	return out, nil
}

func (r sqlRepository) InsertPersonaOwnedCar(id int, car models.OwnedCar) error {
	tx, err := r.db.Beginx()
	if err != nil {
		return err
	}
	res, err := tx.NamedExec("insert into custom_cars (name, is_preset, base_car, car_class, level, physics_profile, rating, resale_price, height_drop, skill_slot_count, version) values (:name, :is_preset, :base_car, :car_class, :level, :physics_profile, :rating, :resale_price, :height_drop, :skill_slot_count, :version)", car.CustomCar)
	if err != nil {
		return err
	}
	ccID, err := res.LastInsertId()
	if err != nil {
		return err
	}
	for _, v := range car.CustomCar.Vinyls {
		v.CustomCar = int(ccID)
		_, err := tx.NamedExec("insert into car_vinyls (custom_car, hash, hue1, hue2, hue3, hue4, layer, mir, rot, sat1, sat2, sat3, sat4, scaleX, scaleY, shear, tranX, tranY, var1, var2, var3, var4) values (:custom_car, :hash, :hue1, :hue2, :hue3, :hue4, :layer, :mir, :rot, :sat1, :sat2, :sat3, :sat4, :scaleX, :scaleY, :shear, :tranX, :tranY, :var1, :var2, :var3, :var4)", v)
		if err != nil {
			return err
		}
	}
	for _, p := range car.CustomCar.Paints {
		p.CustomCar = int(ccID)
		_, err := tx.NamedExec("insert into car_paints (custom_car, `group`, hue, sat, slot, var) values (:custom_car, :group, :hue, :sat, :slot, :var)", p)
		if err != nil {
			return err
		}
	}
	for _, p := range car.CustomCar.VisualParts {
		p.CustomCar = int(ccID)
		_, err := tx.NamedExec("insert into car_vis_paints (custom_car, part_hash, slot_hash) values (:custom_car, :part_hash, :slot_hash)", p)
		if err != nil {
			return err
		}
	}
	for _, p := range car.CustomCar.PerformanceParts {
		p.CustomCar = int(ccID)
		_, err := tx.NamedExec("insert into car_perf_parts (custom_car, hash) values (:custom_car, :hash)", p)
		if err != nil {
			return err
		}
	}
	car.CustomCarID = int(ccID)
	res, err = tx.NamedExec("insert into owned_cars (durability, exp_date, heat, custom_car) values (:durability, :exp_date, :heat, :custom_car)", car)
	if err != nil {
		return err
	}
	ocID, err := res.LastInsertId()
	if err != nil {
		return err
	}
	_, err = tx.Exec("insert into car_slots (persona, owned_car) values (?, ?)", id, int(ocID))
	if err != nil {
		return err
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	return nil
}
