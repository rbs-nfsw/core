package persona

import (
	"github.com/shopspring/decimal"
	"gitlab.com/rbs-nfsw/core/models"
)

type Usecase interface {
	CreatePersona(user int, name string, icon int) (models.Persona, error)
	GetUserPersonas(user int) ([]models.Persona, error)
	GetPersonaByID(id int) (models.Persona, error)
	GetPersonaOwnedCars(id int) ([]models.OwnedCar, error)
	InsertPersonaOwnedCar(id int, car models.OwnedCar) error
}

func NewUsecase(r Repository) Usecase {
	return usecase{
		Repository: r,
	}
}

type usecase struct {
	Repository
}

func (u usecase) CreatePersona(user int, name string, icon int) (models.Persona, error) {
	p := models.Persona{
		Name:   name,
		Icon:   icon,
		Cash:   decimal.NewFromFloat(250000),
		UserID: user,
	}
	id, err := u.InsertPersona(p)
	p.ID = id
	return p, err
}

func (u usecase) GetUserPersonas(user int) ([]models.Persona, error) {
	return u.Repository.SelectUserPersonas(user)
}
