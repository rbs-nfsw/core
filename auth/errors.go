package auth

import "errors"

var ErrAuthFailure = errors.New("auth failure")
