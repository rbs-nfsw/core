package auth

import (
	"crypto/rand"
	"database/sql"
	"encoding/hex"

	"gitlab.com/rbs-nfsw/core/models"
	"gitlab.com/rbs-nfsw/core/util"
)

type Usecase interface {
	VerifyToken(token string, user int) (bool, error)
	AuthenticateUserLegacy(ip string, email string, password string) (int, string, error)
}

func NewUsecase(r Repository) Usecase {
	return usecase{
		repo: r,
	}
}

type usecase struct {
	repo Repository
}

func (u usecase) VerifyToken(token string, user int) (bool, error) {
	tok, err := u.repo.GetToken(token)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}

	if tok.UserID != user {
		return false, nil
	}
	return true, nil
}

func (u usecase) AuthenticateUserLegacy(ip string, email string, password string) (int, string, error) {
	user, err := u.repo.GetUserByEmail(email)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, "", ErrAuthFailure
		}
		return 0, "", err
	}

	if !util.ConstantTimeStringCompare(*user.LegacyPassword, password) {
		return 0, "", ErrAuthFailure
	}

	token := make([]byte, 32)
	rand.Read(token)
	hexToken := hex.EncodeToString(token)
	err = u.repo.InsertToken(models.Token{
		Token:  hexToken,
		UserID: user.ID,
	})
	if err != nil {
		return 0, "", err
	}

	u.repo.UpdateUserLoggedInTime(user.ID)
	u.repo.UpdateUserIP(user.ID, ip)

	return user.ID, hexToken, nil
}
