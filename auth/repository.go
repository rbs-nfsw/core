package auth

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/rbs-nfsw/core/models"
)

type Repository interface {
	GetToken(token string) (models.Token, error)
	GetUserByEmail(email string) (models.User, error)
	InsertToken(token models.Token) error
	UpdateUserLoggedInTime(user int) error
	UpdateUserIP(user int, ip string) error
}

type sqlRepository struct {
	db *sqlx.DB
}

func NewSQLRepository(db *sqlx.DB) Repository {
	return sqlRepository{
		db: db,
	}
}

func (r sqlRepository) GetToken(token string) (models.Token, error) {
	var out models.Token
	err := r.db.Get(&out, "select * from tokens where token = ?", token)
	return out, err
}

func (r sqlRepository) GetUserByEmail(email string) (models.User, error) {
	var out models.User
	err := r.db.Get(&out, "select * from users where email = ?", email)
	return out, err
}

func (r sqlRepository) InsertToken(token models.Token) error {
	_, err := r.db.NamedExec("insert into tokens (token, user_id) values (:token, :user_id)", token)
	return err
}

func (r sqlRepository) UpdateUserLoggedInTime(user int) error {
	_, err := r.db.Exec("update users set last_logged_in = NOW() where id = ?", user)
	return err
}

func (r sqlRepository) UpdateUserIP(user int, ip string) error {
	_, err := r.db.Exec("update users set ip = ? where id = ?", ip, user)
	return err
}
