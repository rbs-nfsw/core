package db

import "gitlab.com/rbs-nfsw/core/models"

func (i Instance) GetUserByEmail(email string) (models.User, error) {
	var out models.User
	err := i.Get(&out, "select * from users where email = ?", email)
	return out, err
}
