package db

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/rbs-nfsw/core/models/db"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func CreateInstance(db *sqlx.DB) Instance {
	return Instance{
		DB: db,
	}
}

type Instance struct {
	*sqlx.DB
}

func (i Instance) GetPersonaNamesWith(prefix string) []string {
	var out []string
	err := i.Select(&out, "SELECT name from personas where name like ? || '%'", prefix)
	check(err)
	return out
}

func (i Instance) InsertToken(token string, uid int) error {
	_, err := i.Exec("insert into tokens (token, user_id) values (?, ?)", token, uid)
	return err
}

func (i Instance) VerifyToken(token string, uid int) bool {
	rows, err := i.Query("SELECT token from tokens where token=? and user_id=?", token, uid)
	check(err)
	defer rows.Close()
	return rows.Next()
}

func (i Instance) GetPersonasForUser(uid int) []db.Persona {
	rows, err := i.Query("SELECT id, name, cash, boost from personas where user_id=?", uid)
	check(err)
	defer rows.Close()
	out := make([]db.Persona, 0)
	for rows.Next() {
		profile := db.Persona{}
		check(rows.Scan(&profile.ID, &profile.Name, &profile.Cash, &profile.Boost))
		profile.UserID = uid
		profile.Presence = 1
		out = append(out, profile)
	}
	return out
}

func (i Instance) GetPersonaByID(id int) db.Persona {
	rows, err := i.Query("SELECT name, cash, boost, uid from personas where id=?", id)
	check(err)
	defer rows.Close()
	profile := db.Persona{}
	if rows.Next() {
		check(rows.Scan(&profile.Name, &profile.Cash, &profile.Boost, &profile.UserID))
		profile.ID = id
		profile.Presence = 1
	}
	return profile
}

func (i Instance) GetCarsForPersona(persona int) []db.OwnedCar {
	cars := make([]db.OwnedCar, 0)
	rows, err := i.Query("select id, durability, heat, basecar, carclasshash, level, physicsprofilehash, rating, resaleprice, rideheightdrop, skillmodslotcount, version, paints, vinyls, name, performanceparts, visualparts from owned_cars where persona_id=?", persona)
	check(err)
	defer rows.Close()
	for rows.Next() {
		trans := db.OwnedCar{}
		rows.Scan(&trans.ID, &trans.Durability, &trans.Heat, &trans.BaseCar, &trans.CarClassHash, &trans.Level, &trans.PhysicsProfileHash, &trans.Rating, &trans.ResalePrice,
			&trans.RideHeightDrop, &trans.SkillModSlotCount, &trans.Version, &trans.Paints, &trans.Vinyls, &trans.Name, &trans.PerformanceParts, &trans.VisualParts)
		cars = append(cars, trans)
	}
	return cars
}

func (i Instance) InsertPersona(persona db.Persona) {
	rows, err := i.Query("insert into personas (user_id, name, cash, boost) values (?, ?, ?, ?)", persona.UserID, persona.Name, persona.Cash, persona.Boost)
	check(err)
	rows.Close()
}

func (i Instance) InsertOwnedCar(car db.OwnedCar) {
	rows, err := i.Query("insert into owned_cars (durability, heat, basecar, carclasshash, level, physicsprofilehash, rating, resaleprice, rideheightdrop, skillmodslotcount, version, persona_id, paints, vinyls, name, performanceparts, visualparts) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17)",
		car.Durability, car.Heat, car.BaseCar, car.CarClassHash, car.Level, car.PhysicsProfileHash, car.Rating, car.ResalePrice,
		car.RideHeightDrop, car.SkillModSlotCount, car.Version, car.PersonaID, car.Paints, car.Vinyls, car.Name, car.PerformanceParts, car.VisualParts)
	check(err)
	rows.Close()
}
