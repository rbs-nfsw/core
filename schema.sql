/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE TABLE IF NOT EXISTS `car_paints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `custom_car` int(10) unsigned NOT NULL,
  `group` int(10) NOT NULL,
  `hue` int(10) NOT NULL,
  `sat` int(10) NOT NULL,
  `slot` int(10) NOT NULL,
  `var` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_car_paints_custom_cars` (`custom_car`),
  CONSTRAINT `FK_car_paints_custom_cars` FOREIGN KEY (`custom_car`) REFERENCES `custom_cars` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `car_perf_parts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `custom_car` int(10) unsigned NOT NULL,
  `hash` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_car_perf_parts_custom_cars` (`custom_car`),
  CONSTRAINT `FK_car_perf_parts_custom_cars` FOREIGN KEY (`custom_car`) REFERENCES `custom_cars` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `car_slots` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona` int(10) unsigned NOT NULL,
  `owned_car` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_car_slots_personas` (`persona`),
  KEY `FK_car_slots_owned_cars` (`owned_car`),
  CONSTRAINT `FK_car_slots_owned_cars` FOREIGN KEY (`owned_car`) REFERENCES `owned_cars` (`id`),
  CONSTRAINT `FK_car_slots_personas` FOREIGN KEY (`persona`) REFERENCES `personas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `car_vinyls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `custom_car` int(11) unsigned NOT NULL,
  `hash` int(11) NOT NULL,
  `hue1` int(11) NOT NULL,
  `hue2` int(11) NOT NULL,
  `hue3` int(11) NOT NULL,
  `hue4` int(11) NOT NULL,
  `layer` int(11) NOT NULL,
  `mir` bit(1) NOT NULL,
  `rot` int(11) NOT NULL,
  `sat1` int(11) NOT NULL,
  `sat2` int(11) NOT NULL,
  `sat3` int(11) NOT NULL,
  `sat4` int(11) NOT NULL,
  `scaleX` int(11) NOT NULL,
  `scaleY` int(11) NOT NULL,
  `shear` int(11) NOT NULL,
  `tranX` int(11) NOT NULL,
  `tranY` int(11) NOT NULL,
  `var1` int(11) NOT NULL,
  `var2` int(11) NOT NULL,
  `var3` int(11) NOT NULL,
  `var4` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_car_vinyls_custom_car` (`custom_car`),
  CONSTRAINT `FK_car_vinyls_custom_car` FOREIGN KEY (`custom_car`) REFERENCES `custom_cars` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `car_vis_parts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `custom_car` int(10) unsigned NOT NULL,
  `part_hash` int(10) NOT NULL,
  `slot_hash` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `perf_parts_fk_custom_car` (`custom_car`),
  CONSTRAINT `FK_car_vis_parts_custom_cars` FOREIGN KEY (`custom_car`) REFERENCES `custom_cars` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `cfg_chat_rooms` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `count` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `short_name` char(2) NOT NULL,
  `long_name` varchar(48) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `cfg_exp_levels` (
  `level` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `points` int(10) unsigned NOT NULL,
  PRIMARY KEY (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `cfg_settings` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `value` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `custom_cars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `is_preset` bit(1) NOT NULL,
  `base_car` int(11) NOT NULL,
  `car_class` int(11) NOT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `physics_profile` int(11) NOT NULL,
  `rating` smallint(5) unsigned NOT NULL,
  `resale_price` decimal(9,2) unsigned NOT NULL,
  `height_drop` float unsigned NOT NULL,
  `skill_slot_count` tinyint(3) unsigned NOT NULL,
  `version` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_custom_cars_personas` (`persona_id`),
  CONSTRAINT `FK_custom_cars_personas` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `owned_cars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `durability` tinyint(3) unsigned NOT NULL DEFAULT 100,
  `exp_date` timestamp NULL DEFAULT NULL,
  `heat` float unsigned NOT NULL,
  `custom_car` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_owned_cars_custom_cars` (`custom_car`),
  CONSTRAINT `FK_owned_cars_custom_cars` FOREIGN KEY (`custom_car`) REFERENCES `custom_cars` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `personas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT 0,
  `name` varchar(16) NOT NULL,
  `icon` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `cash` decimal(9,2) unsigned NOT NULL,
  `boost` decimal(9,2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `tokens` (
  `token` varchar(64) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`token`),
  KEY `FK_tokens_users` (`user_id`),
  CONSTRAINT `FK_tokens_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `legacy_password` char(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `ip` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `last_logged_in` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `admin` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `cfg_chat_rooms` DISABLE KEYS */;
INSERT INTO `cfg_chat_rooms` (`id`, `count`, `short_name`, `long_name`) VALUES
	(1, 2, 'EN', 'TXT_CHAT_LANG_ENGLISH'),
	(2, 2, 'DE', 'TXT_CHAT_LANG_GERMAN'),
	(3, 2, 'FR', 'TXT_CHAT_LANG_FRENCH'),
	(4, 2, 'ES', 'TXT_CHAT_LANG_SPANISH'),
	(5, 2, 'PL', 'TXT_CHAT_LANG_POLISH'),
	(6, 2, 'BR', 'TXT_CHAT_LANG_BRAZILIANPORTUGUESE'),
	(7, 2, 'RU', 'TXT_CHAT_LANG_RUSSIAN'),
	(8, 2, 'GN', 'TXT_CHAT_LANG_GENERAL');
/*!40000 ALTER TABLE `cfg_chat_rooms` ENABLE KEYS */;

/*!40000 ALTER TABLE `cfg_exp_levels` DISABLE KEYS */;
INSERT INTO `cfg_exp_levels` (`level`, `points`) VALUES
	(1, 100),
	(2, 975),
	(3, 2025),
	(4, 3625),
	(5, 5875),
	(6, 8875),
	(7, 12725),
	(8, 17525),
	(9, 23375),
	(10, 30375),
	(11, 39375),
	(12, 50575),
	(13, 64175),
	(14, 80375),
	(15, 99375),
	(16, 121375),
	(17, 146575),
	(18, 175175),
	(19, 207375),
	(20, 243375),
	(21, 283375),
	(22, 327575),
	(23, 376175),
	(24, 429375),
	(25, 487375),
	(26, 550375),
	(27, 618575),
	(28, 692175),
	(29, 771375),
	(30, 856375),
	(31, 950875),
	(32, 1055275),
	(33, 1169975),
	(34, 1295375),
	(35, 1431875),
	(36, 1579875),
	(37, 1739775),
	(38, 1911975),
	(39, 2096875),
	(40, 2294875),
	(41, 2506375),
	(42, 2731775),
	(43, 2971475),
	(44, 3225875),
	(45, 3495375),
	(46, 3780375),
	(47, 4081275),
	(48, 4398475),
	(49, 4732375),
	(50, 5083375),
	(51, 5481355),
	(52, 5898805),
	(53, 6336165),
	(54, 6793875),
	(55, 7272375),
	(56, 7772105),
	(57, 8293505),
	(58, 8837015),
	(59, 9403075),
	(60, 9992125),
	(61, 10581175),
	(62, 11170225),
	(63, 11759275),
	(64, 12348325),
	(65, 12937375),
	(66, 13526425),
	(67, 14115475),
	(68, 14704525),
	(69, 15293575),
	(70, 15882625),
	(71, 17829625);
/*!40000 ALTER TABLE `cfg_exp_levels` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
