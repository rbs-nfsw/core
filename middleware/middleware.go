package middleware

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"gitlab.com/rbs-nfsw/core/auth"
)

type Context struct {
	Auth auth.Usecase
}

func LogMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		err := next(c)
		if err == echo.ErrNotFound {
			c.Response().WriteHeader(http.StatusOK)
			logrus.WithFields(logrus.Fields{
				"method": c.Request().Method,
				"path":   c.Request().URL.String(),
			}).Warn("Request unimplemented")
			return nil
		}
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"method": c.Request().Method,
				"path":   c.Request().URL.String(),
			}).Error("Error while handling request")
			return err
		}
		logrus.WithFields(logrus.Fields{
			"method": c.Request().Method,
			"path":   c.Request().URL.String(),
		}).Info("Request handled")
		return nil
	}
}

func (mc Context) AuthMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		uidS := c.Request().Header.Get("userId")
		if uidS == "" {
			c.Response().WriteHeader(http.StatusBadRequest)
			return nil
		}
		token := c.Request().Header.Get("securityToken")
		if token == "" {
			c.Response().WriteHeader(http.StatusUnauthorized)
			return nil
		}
		uid, _ := strconv.Atoi(uidS)
		ok, err := mc.Auth.VerifyToken(token, uid)
		if err != nil {
			return err
		}

		if !ok {
			c.Response().WriteHeader(http.StatusUnauthorized)
			return nil
		}
		c.Set("userid", uid)
		c.Set("token", token)
		return next(c)
	}
}
