package routes

import (
	"github.com/labstack/echo"

	"gitlab.com/rbs-nfsw/core/auth"
	"gitlab.com/rbs-nfsw/core/config"
	"gitlab.com/rbs-nfsw/core/middleware"
)

type RouteContext struct {
	Auth   auth.Usecase
	Mw     middleware.Context
	Config config.Usecase
}

func (rc RouteContext) Routes(e *echo.Group) {
	e.GET("/User/authenticateUser", rc.authenticateUser)
	e.GET("/GetServerInformation", rc.getServerInformation)
	e.POST("/Reporting/SendHardwareInfo", rc.sendHardwareInfo, rc.Mw.AuthMiddleware)
	e.GET("/Session/GetChatInfo", rc.getChatInfo)
	e.GET("/DriverPersona/GetExpLevelPointsMap", rc.getExpLevelsPointsMap)
}
