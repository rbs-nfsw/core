package routes

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"

	"gitlab.com/rbs-nfsw/core/auth"
	"gitlab.com/rbs-nfsw/core/models"
)

func (rc RouteContext) authenticateUser(c echo.Context) error {
	var request struct {
		Email    string `query:"email"`
		Password string `query:"password"`
	}
	if err := c.Bind(&request); err != nil {
		return err
	}
	userid, token, err := rc.Auth.AuthenticateUserLegacy(c.RealIP(), request.Email, request.Password)
	if err != nil {
		var desc string
		if err == auth.ErrAuthFailure {
			desc = "LOGIN ERROR"
		} else {
			logrus.WithError(err).Error("Error handling request")
			desc = "INTERNAL SERVER ERROR"
		}
		return c.XML(http.StatusInternalServerError, models.LoginStatusVO{
			Description: desc,
		})
	}
	return c.XML(http.StatusOK, models.LoginStatusVO{
		UserID:     userid,
		LoginToken: token,
	})
}
