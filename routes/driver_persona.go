package routes

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/rbs-nfsw/core/models"
)

func (rc RouteContext) getExpLevelsPointsMap(c echo.Context) error {
	mp, err := rc.Config.GetExpLevelsMap()
	if err != nil {
		return err
	}
	return c.XML(http.StatusOK, models.ArrayOfint{mp.AsArray()})
}
