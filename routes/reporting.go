package routes

import (
	"encoding/xml"
	"fmt"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"gitlab.com/rbs-nfsw/core/models"
)

func (rc RouteContext) sendHardwareInfo(c echo.Context) error {
	var hwinfo models.HardwareInfo
	dec := xml.NewDecoder(c.Request().Body)
	err := dec.Decode(&hwinfo)
	if err != nil {
		return err
	}
	logrus.WithField("hwinfo", fmt.Sprintf("%#v", hwinfo)).Info("Received HW Info")
	return nil
}
