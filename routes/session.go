package routes

import (
	"net/http"

	"github.com/labstack/echo"
)

func (rc RouteContext) getChatInfo(c echo.Context) error {
	info, err := rc.Config.GetChatInfo()
	if err != nil {
		return err
	}
	return c.XML(http.StatusOK, info)
}
