package routes

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/rbs-nfsw/core/models"
)

func (rc RouteContext) getServerInformation(c echo.Context) error {
	return c.JSON(http.StatusOK, models.ServerInformation{
		ServerName: "go test server",
		ActivatedSceneryGroups: []string{
			"SCENERY_GROUP_CHRISTMAS", "SCENERY_GROUP_NEWYEARS", "SCENERY_GROUP_OKTOBERFEST", "SCENERY_GROUP_HALLOWEEN",
		},
		DisactivatedSceneryGroups: []string{
			"SCENERY_GROUP_CHRISTMAS_DISABLE", "SCENERY_GROUP_NEWYEARS_DISABLE", "SCENERY_GROUP_HALLOWEEN_DISABLE", "SCENERY_GROUP_OKTOBERFEST_DISABLE",
		},
		Version: "rbs-nfsw/core",
	})
}
