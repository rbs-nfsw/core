package util

import (
	"crypto/subtle"
)

func ConstantTimeStringCompare(x string, y string) bool {
	return subtle.ConstantTimeCompare([]byte(x), []byte(y)) == 1
}
