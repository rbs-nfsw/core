package config

import (
	"strconv"

	"github.com/shopspring/decimal"
	"gitlab.com/rbs-nfsw/core/models"
)

type Usecase interface {
	GetStringParam(param string) (string, error)
	GetIntParam(param string) (int, error)
	GetDecimalParam(param string) (decimal.Decimal, error)
	GetChatInfo() (models.ChatInfo, error)
	GetExpLevelsMap() (models.ExpLevelsMap, error)
}

func NewUsecase(r Repository) Usecase {
	return usecase{
		repo: r,
	}
}

type usecase struct {
	repo Repository
}

func (u usecase) GetStringParam(param string) (string, error) {
	return u.repo.GetParam(param)
}

func (u usecase) GetIntParam(param string) (int, error) {
	v, err := u.repo.GetParam(param)
	if err != nil {
		return 0, err
	}
	return strconv.Atoi(v)
}

func (u usecase) GetDecimalParam(param string) (decimal.Decimal, error) {
	v, err := u.repo.GetParam(param)
	if err != nil {
		return decimal.Zero, err
	}
	return decimal.NewFromString(v)
}

func (u usecase) GetChatInfo() (models.ChatInfo, error) {
	rooms, err := u.repo.GetChatRooms()
	if err != nil {
		return models.ChatInfo{}, err
	}
	ip, err := u.GetStringParam("xmpp.ip")
	if err != nil {
		return models.ChatInfo{}, err
	}
	port, err := u.GetIntParam("xmpp.port")
	if err != nil {
		return models.ChatInfo{}, err
	}
	return models.ChatInfo{
		Rooms:  rooms,
		IP:     ip,
		Port:   port,
		Prefix: "smga",
	}, nil
}

func (u usecase) GetExpLevelsMap() (models.ExpLevelsMap, error) {
	return u.repo.GetExpLevelsMap()
}
