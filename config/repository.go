package config

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/rbs-nfsw/core/models"
)

type Repository interface {
	GetParam(p string) (string, error)
	GetChatRooms() ([]models.ChatRoom, error)
	GetExpLevelsMap() (models.ExpLevelsMap, error)
}

type sqlRepository struct {
	db *sqlx.DB
}

func NewSQLRepository(db *sqlx.DB) Repository {
	return sqlRepository{
		db: db,
	}
}

func (r sqlRepository) GetParam(p string) (string, error) {
	var res string
	err := r.db.Get(&res, "select value from cfg_settings where name = ?", p)
	return res, err
}

func (r sqlRepository) GetChatRooms() ([]models.ChatRoom, error) {
	var res []models.ChatRoom
	err := r.db.Select(&res, "select * from cfg_chat_rooms")
	return res, err
}

func (r sqlRepository) GetExpLevelsMap() (models.ExpLevelsMap, error) {
	var res models.ExpLevelsMap
	err := r.db.Select(&res, "select * from cfg_exp_levels")
	return res, err
}
