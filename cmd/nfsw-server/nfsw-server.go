package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"time"

	"gitlab.com/rbs-nfsw/core/auth"
	"gitlab.com/rbs-nfsw/core/config"
	"gitlab.com/rbs-nfsw/core/models"
	"gitlab.com/rbs-nfsw/core/persona"

	"github.com/iancoleman/strcase"

	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"

	"github.com/beevik/etree"
	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"gitlab.com/rbs-nfsw/core/db"
	lmw "gitlab.com/rbs-nfsw/core/middleware"
	xmlm "gitlab.com/rbs-nfsw/core/models/xml"
	"gitlab.com/rbs-nfsw/core/routes"
)

var (
	dbi           db.Instance
	dbNumberRegex = regexp.MustCompile("_(\\d)+")
)

type user struct {
	FullGameAccess bool   `xml:"fullGameAccess"`
	Complete       bool   `xml:"isComplete"`
	RemoteUserID   int    `xml:"remoteUserId"`
	SecurityToken  string `xml:"securityToken"`
	SubscribeMsg   bool   `xml:"subscribeMsg"`
	UserID         int    `xml:"userId"`
}

type personaList struct {
	Personas []models.ProfileData
}

type userInfo struct {
	XMLName           xml.Name    `xml:"UserInfo"`
	DefaultPersonaIdx int         `xml:"defaultPersonaIdx"`
	PersonaList       personaList `xml:"personas"`
	User              user        `xml:"user"`
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	check(godotenv.Load())

	e := echo.New()

	e.Use(middleware.Recover())
	e.Use(middleware.Gzip())

	_db := sqlx.MustConnect(os.Getenv("SMGA_DB_DRIVER"), os.Getenv("SMGA_DB_DSN"))
	_db.SetMaxOpenConns(15)
	_db.MapperFunc(func(s string) string {
		s = strcase.ToSnake(s)
		return dbNumberRegex.ReplaceAllString(s, "$1")
	})
	dbi = db.CreateInstance(_db)

	ar := auth.NewSQLRepository(_db)
	au := auth.NewUsecase(ar)
	pr := persona.NewSQLRepository(_db)
	pu := persona.NewUsecase(pr)
	cr := config.NewSQLRepository(_db)
	cu := config.NewUsecase(cr)

	mctx := lmw.Context{
		Auth: au,
	}
	ctx := routes.RouteContext{
		Auth:   au,
		Mw:     mctx,
		Config: cu,
	}

	ng := e.Group("/soapbox-race-core/Engine.svc")
	ng.Use(lmw.LogMiddleware)
	ctx.Routes(ng)
	ng.POST("/User/GetPermanentSession", func(c echo.Context) error {
		personas, err := pu.GetUserPersonas(c.Get("userid").(int))
		if err != nil {
			return err
		}
		out := make([]models.ProfileData, len(personas))
		for i, p := range personas {
			out[i] = p.AsProfileData()
		}
		u := user{
			SecurityToken: c.Get("token").(string),
			UserID:        c.Get("userid").(int),
		}
		c.XML(http.StatusOK, userInfo{PersonaList: personaList{out}, User: u})
		return nil
	}, mctx.AuthMiddleware)
	ng.GET("/getusersettings", func(c echo.Context) error {
		out := fmt.Sprintf(`<User_Settings>
			<CarCacheAgeLimit>600</CarCacheAgeLimit>
			<IsRaceNowEnabled>true</IsRaceNowEnabled>
			<MaxCarCacheSize>250</MaxCarCacheSize>
			<MinRaceNowLevel>2</MinRaceNowLevel>
			<VoipAvailable>false</VoipAvailable>
			<activatedHolidaySceneryGroups>
				<string>SCENERY_GROUP_NORMAL</string>
				<string>SCENERY_GROUP_CHRISTMAS</string>
			</activatedHolidaySceneryGroups>
			<activeHolidayIds>
				<long>0</long>
				<long>3</long>
			</activeHolidayIds>
			<disactivatedHolidaySceneryGroups>
				<string>SCENERY_GROUP_NORMAL_DISABLE</string>
				<string>SCENERY_GROUP_CHRISTMAS_DISABLE</string>
			</disactivatedHolidaySceneryGroups>
			<firstTimeLogin>false</firstTimeLogin>
			<maxLevel>60</maxLevel>
			<starterPackApplied>false</starterPackApplied>
			<userId>%v</userId>
		</User_Settings>`, c.Get("userid").(int))
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, out)
	}, mctx.AuthMiddleware)
	ng.GET("/carclasses", func(c echo.Context) error {
		return c.File("carclasses.xml")
	}, mctx.AuthMiddleware)
	ng.GET("/systeminfo", func(c echo.Context) error {
		out := "<SystemInfo xmlns=\"http://schemas.datacontract.org/2004/07/EA.NFSWO.ENGINE.Service\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">" +
			"<Branch>production</Branch>" +
			"<ChangeList>620384</ChangeList>" +
			"<ClientVersion>637</ClientVersion>" +
			"<ClientVersionCheck>true</ClientVersionCheck>" +
			"<Deployed>12/30/2070 20:00:70</Deployed>" +
			"<EntitlementsToDownload>false</EntitlementsToDownload>" +
			"<EntitlementsToPlay>true</EntitlementsToPlay>" +
			"<ForcePermanentSession>true</ForcePermanentSession>" +
			"<JidPrepender>smga</JidPrepender>" +
			"<LauncherServiceUrl/>" +
			"<NucleusNamespace>nfsw-ob</NucleusNamespace>" +
			"<NucleusNamespaceWeb>nfs_web</NucleusNamespaceWeb>" +
			"<PortalDomain/>" +
			"<PortalSecureDomain/>" +
			"<PortalTimeOut>10000</PortalTimeOut>" +
			"<Time>" + time.Now().Format("2006-01-02T15:04") + ":00.0000000+00:00</Time>" +
			"<Version>1599</Version>" +
			"</SystemInfo>"
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, out)
	}, mctx.AuthMiddleware)
	ng.GET("/getrebroadcasters", func(c echo.Context) error {
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, "<ArrayOfUdpRelayInfo><UdpRelayInfo><Host>localhost</Host><Port>9999</Port></UdpRelayInfo></ArrayOfUdpRelayInfo>")
	}, mctx.AuthMiddleware)
	ng.GET("/getregioninfo", func(c echo.Context) error {
		out := "<RegionInfo>" +
			"<CountdownProposalInMilliseconds>3000</CountdownProposalInMilliseconds>" +
			"<DirectConnectTimeoutInMilliseconds>1000</DirectConnectTimeoutInMilliseconds>" +
			"<DropOutTimeInMilliseconds>15000</DropOutTimeInMilliseconds>" +
			"<EventLoadTimeoutInMilliseconds>30000</EventLoadTimeoutInMilliseconds>" +
			"<HeartbeatIntervalInMilliseconds>1000</HeartbeatIntervalInMilliseconds>" +
			"<UdpRelayBandwidthInBps>9600</UdpRelayBandwidthInBps>" +
			"<UdpRelayTimeoutInMilliseconds>60000</UdpRelayTimeoutInMilliseconds>" +
			"</RegionInfo>"
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, out)
	}, mctx.AuthMiddleware)
	ng.GET("/LoginAnnouncements", func(c echo.Context) error {
		doc := etree.NewDocument()
		doc.CreateElement("LoginAnnouncementsDefinition")
		out, _ := doc.WriteToString()

		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, out)
	}, mctx.AuthMiddleware)
	ng.GET("/catalog/productsInCategory", func(c echo.Context) error {
		return c.XML(http.StatusOK, xmlm.LoadProductCategory(c.QueryParam("categoryName")))
	}, mctx.AuthMiddleware)
	ng.POST("/DriverPersona/ReserveName", func(c echo.Context) error {
		name := c.QueryParam("name")
		doc := etree.NewDocument()
		array := doc.CreateElement("ArrayOfString")
		namesWith := dbi.GetPersonaNamesWith(name)
		if isIn(namesWith, name) {
			end := 1
			for {
				if !isIn(namesWith, name+strconv.Itoa(end)) {
					break
				}
				end++
			}
			array.CreateElement("string").SetText(name + strconv.Itoa(end))
		}
		res, _ := doc.WriteToString()
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, res)
	}, mctx.AuthMiddleware)
	ng.POST("/DriverPersona/CreatePersona", func(c echo.Context) error {
		id, _ := strconv.Atoi(c.QueryParam("userId"))
		icon, _ := strconv.Atoi(c.QueryParam("iconIndex"))
		persona, err := pu.CreatePersona(id, c.QueryParam("name"), icon)
		if err != nil {
			return err
		}
		return c.XML(http.StatusOK, persona.AsProfileData())
	}, mctx.AuthMiddleware)
	ng.POST("/personas/:persona/baskets", func(c echo.Context) error {
		inDoc := etree.NewDocument()
		body := c.Request().Body
		bodyB, _ := ioutil.ReadAll(body)
		inDoc.ReadFromBytes(bodyB)
		productID := inDoc.SelectElement("BasketTrans").SelectElement("Items").SelectElement("BasketItemTrans").SelectElement("ProductId").Text()
		persona, _ := strconv.Atoi(c.Param("persona"))
		trans := models.GetCarBasket(productID)
		err := pu.InsertPersonaOwnedCar(persona, trans)
		if err != nil {
			return err
		}
		out := "<CommerceResultTrans>" +
			"<CommerceItems/>" +
			"<InventoryItems>" +
			"<InventoryItemTrans>" +
			"<Hash>0</Hash>" +
			"<InventoryId>0</InventoryId>" +
			"<RemainingUseCount>0</RemainingUseCount>" +
			"<ResellPrice>0.0</ResellPrice>" +
			"</InventoryItemTrans>" +
			"</InventoryItems>" +
			"<PurchasedCars>" +
			"<OwnedCarTrans>" +
			"<Durability>0</Durability>" +
			"<Heat>0.0</Heat>" +
			"<Id>0</Id>" +
			"</OwnedCarTrans>" +
			"</PurchasedCars>" +
			"<Status>Success</Status>" +
			"<Wallets>" +
			"<WalletTrans>" +
			"<Balance>5000</Balance>" +
			"<Currency>CASH</Currency>" +
			"</WalletTrans>" +
			"</Wallets>" +
			"</CommerceResultTrans>"
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, out)
	}, mctx.AuthMiddleware)
	ng.GET("/personas/:persona/carslots", func(c echo.Context) error {
		persona, _ := strconv.Atoi(c.Param("persona"))
		cars, err := pu.GetPersonaOwnedCars(persona)
		if err != nil {
			return err
		}
		out := models.CarSlotInfoTrans{
			CarsOwnedByPersona:   cars,
			DefaultOwnedCarIndex: 0,
			OwnedCarSlotsCount:   200,
		}
		return c.XMLPretty(http.StatusOK, out, "    ")
	}, mctx.AuthMiddleware)
	ng.GET("/personas/:persona/defaultcar", func(c echo.Context) error {
		persona, _ := strconv.Atoi(c.Param("persona"))
		cars, _ := pu.GetPersonaOwnedCars(persona)
		if len(cars) > 0 {
			return c.XML(http.StatusOK, cars[0])
		}
		return c.String(http.StatusOK, "")
	}, mctx.AuthMiddleware)
	ng.GET("/personas/inventory/objects", func(c echo.Context) error {
		return c.XML(http.StatusOK, xmlm.InventoryTrans{})
	}, mctx.AuthMiddleware)
	ng.GET("/achievements/loadall", func(c echo.Context) error {
		return c.XML(http.StatusOK, xmlm.AchievementsPacket{})
	}, mctx.AuthMiddleware)
	ng.GET("/events/availableatlevel", func(c echo.Context) error {
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.File("availableatlevel.xml")
	}, mctx.AuthMiddleware)
	ng.POST("/Gifts/GetAndTriggerAvailableLevelGifts", func(c echo.Context) error {
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, "<ArrayOfLevelGiftDefinition/>")
	}, mctx.AuthMiddleware)
	ng.GET("/getblockeduserlist", func(c echo.Context) error {
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, "<ArrayOfLong/>")
	}, mctx.AuthMiddleware)
	ng.GET("/getblockersbyusers", func(c echo.Context) error {
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, "<ArrayOfLong/>")
	}, mctx.AuthMiddleware)
	ng.GET("/getsocialsettings", func(c echo.Context) error {
		out := `<SocialSettings>
    <AppearOffline>false</AppearOffline>
    <DeclineGroupInvite>0</DeclineGroupInvite>
    <DeclineIncommingFriendRequests>false</DeclineIncommingFriendRequests>
    <DeclinePrivateInvite>0</DeclinePrivateInvite>
    <HideOfflineFriends>false</HideOfflineFriends>
    <ShowNewsOnSignIn>false</ShowNewsOnSignIn>
    <ShowOnlyPlayersInSameChatChannel>false</ShowOnlyPlayersInSameChatChannel>
</SocialSettings>`
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, out)
	}, mctx.AuthMiddleware)
	ng.GET("/security/fraudConfig", func(c echo.Context) error {
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, fmt.Sprintf(`<FraudConfig>
			<enabledBitField>12</enabledBitField>
			<gameFileFreq>1000000</gameFileFreq>
			<moduleFreq>360000</moduleFreq>
			<startUpFreq>1000000</startUpFreq>
			<userID>%v</userID>
		</FraudConfig>`, c.Get("userid").(int)))
	}, mctx.AuthMiddleware)
	ng.GET("/getfriendlistfromuserid", func(c echo.Context) error {
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, `<PersonaFriendsList xsi:schemaLocation="http://schemas.datacontract.org/2004/07/Victory.TransferObjects.DriverPersona" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		</PersonaFriendsList>`)
	}, mctx.AuthMiddleware)
	ng.GET("/logging/client", func(c echo.Context) error {
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, `<ClientConfigTrans xsi:schemaLocation="http://schemas.datacontract.org/2004/07/Victory.DataLayer.Serialization" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>`)
	}, mctx.AuthMiddleware)
	ng.POST("/DriverPersona/GetPersonaBaseFromList", func(c echo.Context) error {
		personas, _ := pu.GetUserPersonas(c.Get("userid").(int))
		out := make([]models.PersonaBase, len(personas))
		for i, p := range personas {
			out[i] = p.AsPersonaBase()
			out[i].Presence = 0
		}
		return c.XMLPretty(http.StatusOK, models.ArrayOfPersonaBase{out}, "    ")
	}, mctx.AuthMiddleware)
	ng.GET("/NewsArticles", func(c echo.Context) error {
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, "<ArrayOfNewsArticleTrans/>")
	}, mctx.AuthMiddleware)
	ng.GET("/DriverPersona/GetPersonaInfo", func(c echo.Context) error {
		personaID, _ := strconv.Atoi(c.QueryParam("personaId"))
		persona, _ := pu.GetPersonaByID(personaID)
		return c.XMLPretty(http.StatusOK, persona.AsProfileData(), "    ")
	}, mctx.AuthMiddleware)
	ng.GET("/crypto/cryptoticket", func(c echo.Context) error {
		c.Response().Header().Set("Content-Type", "application/xml")
		return c.String(http.StatusOK, `<ClientServerCryptoTicket>
			<CryptoTicket>CgsMDQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=</CryptoTicket>
			<SessionKey>AAAAAAAAAAAAAAAAAAAAAA==</SessionKey>
			<TicketIv>AAAAAAAAAAAAAAAAAAAAAA==</TicketIv>
			</ClientServerCryptoTicket>`)
	}, mctx.AuthMiddleware)

	e.Start(":1338")
}

func isIn(a []string, v string) bool {
	for _, av := range a {
		if av == v {
			return true
		}
	}
	return false
}

type basketTrans struct {
	XMLName xml.Name `xml:"BasketTrans"`
}
